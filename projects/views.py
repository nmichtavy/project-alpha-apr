from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProject

# Create your views here.


@login_required
def list_projects(request):
    all_projects = Project.objects.filter(owner=request.user)
    context = {"list_projects": all_projects}
    return render(request, "projects/list_projects.html", context)


def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {
        "project_object": project,
    }
    return render(request, "projects/project_detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProject(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/projects/")
    else:
        form = CreateProject()
    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)
