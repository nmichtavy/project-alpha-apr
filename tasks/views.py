from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import CreateTask
from tasks.models import Task

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTask(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/projects/")
    else:
        form = CreateTask()
    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def show_my_tasks(request):
    assignee_tasks = Task.objects.filter(assignee=request.user)
    context = {"assignee_tasks": assignee_tasks}
    return render(request, "tasks/show_my_tasks.html", context)
